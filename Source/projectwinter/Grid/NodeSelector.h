// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NodeSelector.generated.h"

class UMyNode;

UCLASS()
class PROJECTWINTER_API ANodeSelector : public AActor
{
	GENERATED_BODY()
	
public:	

	ANodeSelector();

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Placement")
	FVector StartLocation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Placement")
	FRotator StartRotation = FRotator::ZeroRotator;

private:

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* DisplayMesh;


public:

	void UpdateState(UMyNode* InputNode);

};
