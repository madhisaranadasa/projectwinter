// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NodeActor.generated.h"

class UMyNode;

UCLASS()
class PROJECTWINTER_API ANodeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ANodeActor();

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* DisplayMesh;

public:

	UMyNode* CurrentNode;

public:

	void InitNodeActor(UMyNode* StartNode);


};
