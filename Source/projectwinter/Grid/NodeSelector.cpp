// Fill out your copyright notice in the Description page of Project Settings.


#include "NodeSelector.h"
#include "MyNode.h"


ANodeSelector::ANodeSelector()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create Root
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create display mesh
	DisplayMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DisplayMesh"));
	DisplayMesh->SetupAttachment(RootComponent);
}


void ANodeSelector::BeginPlay()
{
	Super::BeginPlay();
	
}


void ANodeSelector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ANodeSelector::UpdateState(UMyNode* InputNode)
{
	SetActorLocation(InputNode->GetWorldPosition());
}

