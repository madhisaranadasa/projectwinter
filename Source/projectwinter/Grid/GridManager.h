// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridManager.generated.h"

class UMyNode;
class ANodeActor;

UCLASS()
class PROJECTWINTER_API AGridManager : public AActor
{
	GENERATED_BODY()
	
public:	

	AGridManager();

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

private:
	
	UPROPERTY(EditAnywhere, Category = "Grid Properties")
	int32 GridSizeW;

	UPROPERTY(EditAnywhere, Category = "Grid Properties")
	int32 GridSizeH;

	UPROPERTY(EditAnywhere, Category = "Grid Properties")
	float NodeSize;

	UPROPERTY(VisibleAnywhere, Category = "Grid Properties")
	TArray<UMyNode*> Grid;

private:

	// Create MyNode objects and add then to the grid array
	void CreateGrid();

public:

	// Return node from a (w,h)
	UMyNode* GetNodeFromCoord(int32 w, int32 h);

	// Return node from a world position
	UMyNode* GetNodeFromPoint(FVector InputPoint);

	// Return (w,h) from a world position
	void GetCoordFromPoint(FVector InputPoint, int32& w, int32& h);

	// Loop through array and draw a debug box
	void DisplayGrid();

	// Attempt to spawn a node actor at the desire coordinates
	void SpawnNodeActor(UMyNode* InputNode, TSubclassOf<ANodeActor> NodeActorAsset);

	// Decouple node actor and couple it to the end node
	void MoveNodeActor(ANodeActor* InputNodeActor, UMyNode* EndNode);

	// Pathfinding algorithm
	void FindPath(UMyNode* StartNode, UMyNode* TargetNode);

	// Return valid neighboring nodes from the input node
	TArray<UMyNode*> GetNeighbors(UMyNode* InputNode);

	// Calculate distance between two nodes
	int32 GetDistance(UMyNode* NodeA, UMyNode* NodeB);

	void RetracePath(UMyNode* StartNode, UMyNode* EndNode);

};
