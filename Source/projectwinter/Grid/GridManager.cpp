// Fill out your copyright notice in the Description page of Project Settings.


#include "GridManager.h"
#include "MyNode.h"
#include "DrawDebugHelpers.h"
#include "GenericPlatform/GenericPlatformMath.h"
#include "NodeActor.h"
//#include <Algo/Sort.h>

// Sets default values
AGridManager::AGridManager()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set up default grid properties
	GridSizeW = 10;
	GridSizeH = 10;
	NodeSize = 100.0f;
}


void AGridManager::BeginPlay()
{
	Super::BeginPlay();

	CreateGrid();
	//DisplayGrid();
}


void AGridManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void AGridManager::CreateGrid()
{
	FVector StartPosition = FVector(GridSizeH / -2 * NodeSize + NodeSize * 0.5f, GridSizeW / -2 * NodeSize + NodeSize * 0.5f, 0.0f);

	for (int32 w = 0; w < GridSizeW; w++)
	{
		for (int32 h = 0; h < GridSizeH; h++)
		{
			FVector NodePosition = FVector(h * NodeSize, w * NodeSize, 0.0f) + StartPosition;
			UMyNode* NewNode = NewObject<UMyNode>();
			NewNode->InitNode(w, h, NodePosition, Cast<AActor>(this));
			Grid.Add(NewNode);
		}
	}
}


void AGridManager::DisplayGrid()
{
	for (int32 w = 0; w < GridSizeW; w++)
	{
		for (int32 h = 0; h < GridSizeH; h++)
		{

			FVector NodePosition = GetNodeFromCoord(w, h)->GetWorldPosition();

			if (GetNodeFromCoord(w, h)->IsWalkable())
			{
				DrawDebugBox(GetWorld(), NodePosition, FVector(10.0f, 10.0f, 10.0f), FColor::Green, false, 3.0f, 0, 3.0f);
			}
			else
			{
				DrawDebugBox(GetWorld(), NodePosition, FVector(50.0f, 50.0f, 50.0f), FColor::Red, false, 3.0f, 0, 3.0f);
			}
		}
	}
}


void AGridManager::SpawnNodeActor(UMyNode* InputNode, TSubclassOf<ANodeActor> NodeActorAsset)
{

	if (!NodeActorAsset)
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't spawn actor no node actor asset is not defined"));
		return;
	}

	if (!InputNode->IsFree())
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't spawn actor because node is not free"));
		return;
	}

	if (!InputNode->IsWalkable())
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't spawn actor because node is not walkable"));
		return;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ANodeActor* SpawnedNodeActor = GetWorld()->SpawnActor<ANodeActor>(NodeActorAsset, InputNode->GetWorldPosition(), FRotator::ZeroRotator, SpawnInfo);
	InputNode->SetNodeActor(SpawnedNodeActor);
}


void AGridManager::MoveNodeActor(ANodeActor* InputNodeActor, UMyNode* EndNode)
{
	UMyNode* StartNode = InputNodeActor->CurrentNode;

	StartNode->ClearNodeActor();
	EndNode->SetNodeActor(InputNodeActor);
}


UMyNode* AGridManager::GetNodeFromCoord(int32 w, int32 h)
{
	return Grid[w * GridSizeH + h];
}


UMyNode* AGridManager::GetNodeFromPoint(FVector InputPoint)
{
	int32 w = FGenericPlatformMath::FloorToInt(InputPoint.Y / NodeSize) + GridSizeW / 2;
	w = FMath::Clamp(w, 0, GridSizeW - 1);

	int32 h = FGenericPlatformMath::FloorToInt(InputPoint.X / NodeSize) + GridSizeH / 2;
	h = FMath::Clamp(h, 0, GridSizeH - 1);

	return GetNodeFromCoord(w, h);

}


void AGridManager::GetCoordFromPoint(FVector InputPoint, int32& w, int32& h)
{
	w = FGenericPlatformMath::FloorToInt(InputPoint.Y / NodeSize) + GridSizeW / 2;
	w = FMath::Clamp(w, 0, GridSizeW - 1);

	h = FGenericPlatformMath::FloorToInt(InputPoint.X / NodeSize) + GridSizeH / 2;
	h = FMath::Clamp(h, 0, GridSizeH - 1);
}


void AGridManager::FindPath(UMyNode* StartNode, UMyNode* TargetNode)
{
	TArray<UMyNode*> OpenSet; // set of nodes to be evaluated
	TArray<UMyNode*> ClosedSet; // set of nodes that have already been evaluated

	OpenSet.Add(StartNode); // to begin, at the starting node to the open set

	while (OpenSet.Num() > 0)
	{
		UMyNode* CurrentNode = OpenSet[0]; // start with the first node in the set

		for (int32 i = 1; i < OpenSet.Num(); i++) // loop through all nodes in the open set to find lowest F Cost (excluding index 0)
		{
			if (OpenSet[i]->GetFCost() < CurrentNode->GetFCost())
			{
				CurrentNode = OpenSet[i]; // set CurrentNode if a lower fCost is found
			}
			else if (OpenSet[i]->GetFCost() == CurrentNode->GetFCost() && OpenSet[i]->GetHCost() < CurrentNode->GetHCost())
			{
				CurrentNode = OpenSet[i]; // set CurrentNode if fCost is same and hCost is lower (closer to target)
			}
		}

		OpenSet.Remove(CurrentNode); // node with lowest F cost has been found, remove from OpenSet and add to ClosedSet
		ClosedSet.Add(CurrentNode);

		if (CurrentNode == TargetNode)
		{
			RetracePath(StartNode, TargetNode);
			return; // path has ended at target, exit loop
		}

		// loop through neighbors of the current node
		for (UMyNode* NeighborNode : GetNeighbors(CurrentNode))
		{
			if (!NeighborNode->IsWalkable() || !NeighborNode->IsFree() || ClosedSet.Contains(NeighborNode))
			{
				continue; //skip neighbor if it is not valid or already in the closed set
			}

			int32 NewMovementCostToNeighbor = CurrentNode->GetGCost() + GetDistance(CurrentNode, NeighborNode);
			if (NewMovementCostToNeighbor < NeighborNode->GetGCost() || !OpenSet.Contains(NeighborNode))
			{
				NeighborNode->SetGCost(NewMovementCostToNeighbor);
				NeighborNode->SetHCost(GetDistance(NeighborNode, TargetNode));
				NeighborNode->SetParent(CurrentNode);

				if (!OpenSet.Contains(NeighborNode))
				{
					OpenSet.Add(NeighborNode);
				}
			}
		}
	}
}


TArray<UMyNode*> AGridManager::GetNeighbors(UMyNode* InputNode)
{
	TArray<UMyNode*> Neighbors;

	for (int32 w = -1; w <= 1; w++)
	{
		for (int32 h = -1; h <= 1; h++)
		{
			if (w == 0 && h == 0)
			{
				continue;
			}

			int32 CheckW = InputNode->GetW() + w;
			int32 CheckH = InputNode->GetH() + h;

			if (CheckW >= 0 && CheckW < GridSizeW && CheckH >= 0 && CheckH < GridSizeH)
			{
				Neighbors.Add(GetNodeFromCoord(CheckW, CheckH));
			}
		}
	}

	return Neighbors;
}


int32 AGridManager::GetDistance(UMyNode* NodeA, UMyNode* NodeB)
{
	int32 DistanceH = FMath::Abs(NodeA->GetH() - NodeB->GetH());
	int32 DistanceW = FMath::Abs(NodeA->GetW() - NodeB->GetW());

	if (DistanceH > DistanceW)
	{
		return 14 * DistanceH - 10 * (DistanceH - DistanceW);
	}
	else
	{
		return 14 * DistanceW - 10 * (DistanceW - DistanceH);
	}
}


void AGridManager::RetracePath(UMyNode* StartNode, UMyNode* EndNode)
{
	TArray<UMyNode*> Path;

	UMyNode* CurrentNode = EndNode;

	while (CurrentNode != StartNode)
	{
		Path.Add(CurrentNode);
		CurrentNode = CurrentNode->GetParent();
	}

	//Algo::Reverse(Path);

	for (UMyNode* Node : Path)
	{
		DrawDebugBox(GetWorld(), Node->GetWorldPosition(), FVector(50.0f, 50.0f, 50.0f), FColor::Red, false, 3.0f, 0, 3.0f);
	}
}
