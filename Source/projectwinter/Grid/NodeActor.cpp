// Fill out your copyright notice in the Description page of Project Settings.


#include "NodeActor.h"
#include "MyNode.h"

// Sets default values
ANodeActor::ANodeActor()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create Root
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create display mesh
	DisplayMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DisplayMesh"));
	DisplayMesh->SetupAttachment(RootComponent);
}


void ANodeActor::BeginPlay()
{
	Super::BeginPlay();
	
}


void ANodeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ANodeActor::InitNodeActor(UMyNode* StartNode)
{
	CurrentNode = StartNode;
}

