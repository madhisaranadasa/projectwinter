// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MyNode.generated.h"

class ANodeActor;

UCLASS()
class PROJECTWINTER_API UMyNode : public UObject
{
	GENERATED_BODY()

public:

	UMyNode();

private:
	
	AActor* WorldContextActor;

	ANodeActor* CurrentNodeActor;

	UMyNode* ParentNode;

	bool bWalkable;

	FVector WorldPosition;

	int32 gCost;

	int32 hCost;

	int32 w;

	int32 h;

	
public:

	// Initialize node with world position and world context
	void InitNode(int32 InputW, int32 InputH, FVector InputPosition, AActor* InputWorldContextActor);

	// Box cast to evaluate if the node is unwalkable
	void CheckCollision();

public:

	// Return if node is walkable
	bool IsWalkable();

	// Return if node is occupied
	bool IsFree();

	// Return world position
	FVector GetWorldPosition();

	// Set world position
	void SetWorldPosition(FVector InputPosition);

	// Return node actor
	ANodeActor* GetNodeActor();

	// Couple Node and NodeActor to reference each other
	void SetNodeActor(ANodeActor* InputNodeActor);

	// Remove references between Node and NodeActor
	void ClearNodeActor();

public:

	int32 GetW();

	int32 GetH();

	int32 GetGCost();

	int32 GetHCost();

	int32 GetFCost();

	void SetGCost(int32 input);

	void SetHCost(int32 input);

	void SetParent(UMyNode* InputNode);

	UMyNode* GetParent();

};
