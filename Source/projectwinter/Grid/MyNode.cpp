// Fill out your copyright notice in the Description page of Project Settings.


#include "MyNode.h"
#include "Kismet/KismetSystemLibrary.h"
#include "NodeActor.h"

UMyNode::UMyNode()
{

}


void UMyNode::InitNode(int32 InputW, int32 InputH, FVector InputPosition, AActor* InputWorldContextActor)
{

	WorldContextActor = InputWorldContextActor;

	// Default settings for an empty node
	bWalkable = true;
	CurrentNodeActor = nullptr;
	w = InputW;
	h = InputH;
	SetWorldPosition(InputPosition);

	// Check collision on initialization
	CheckCollision();
}


void UMyNode::CheckCollision()
{

	//see: https://gist.github.com/JoshLmao/723bde5dd4b8c650c116e0d2ada66cce

	// Define collision channel
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_WorldStatic));

	// Filter to specific classes
	UClass* ClassFilter = AActor::StaticClass(); // NULL;

	// List of actors to ignore
	TArray<AActor*> IgnoreActors;

	// Holds all qualifying actors
	TArray<AActor*> OutActors;

	// Perform the box overlap
	bool bOverlap = UKismetSystemLibrary::BoxOverlapActors(WorldContextActor->GetWorld(), WorldPosition, FVector(50.0f, 50.0f, 50.0f), TraceObjectTypes, ClassFilter, IgnoreActors, OutActors);

	// Update walkable status
	bWalkable = !bOverlap;
}


bool UMyNode::IsWalkable()
{
	return bWalkable;
}


bool UMyNode::IsFree()
{
	if (CurrentNodeActor)
	{
		return false;
	}
	else
	{
		return true;
	}
}


FVector UMyNode::GetWorldPosition()
{
	return WorldPosition;
}


void UMyNode::SetWorldPosition(FVector InputPosition)
{
	WorldPosition = InputPosition;
}


ANodeActor* UMyNode::GetNodeActor()
{
	return CurrentNodeActor;
}


void UMyNode::SetNodeActor(ANodeActor* InputNodeActor)
{
	// Set link on this
	CurrentNodeActor = InputNodeActor;
	// Set link on corresponding actor
	InputNodeActor->CurrentNode = this;
	// Update node actor position
	CurrentNodeActor->SetActorLocation(WorldPosition);
}


void UMyNode::ClearNodeActor()
{
	// clear link on correspond actor
	CurrentNodeActor->CurrentNode = nullptr;
	// clear link on this (must occur last)
	CurrentNodeActor = nullptr;
}


int32 UMyNode::GetW()
{
	return w;
}

int32 UMyNode::GetH()
{
	return h;
}

int32 UMyNode::GetGCost()
{
	return gCost;
}


int32 UMyNode::GetHCost()
{
	return hCost;
}


int32 UMyNode::GetFCost()
{
	return gCost + hCost;
}


void UMyNode::SetGCost(int32 input)
{
	gCost = input;
}


void UMyNode::SetHCost(int32 input)
{
	hCost = input;
}


void UMyNode::SetParent(UMyNode* InputNode)
{
	ParentNode = InputNode;
}

UMyNode* UMyNode::GetParent()
{
	return ParentNode;
}
