// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SelectionManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTWINTER_API USelectionManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	USelectionManager();

protected:

	virtual void BeginPlay() override;


};
