// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyPlayerController.h"
#include "Components/ActorComponent.h"
#include "DisplayManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdatePlayerStateDelegate, EPlayerState, CurrentState);

class AMyCamera;
class AMyPlayerController;
class ANodeSelector;
class UMyNode;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTWINTER_API UDisplayManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UDisplayManager();

protected:

	virtual void BeginPlay() override;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Asset References")
	TSubclassOf<AMyCamera> CameraPawnAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Asset References")
	TSubclassOf<UUserWidget> HUDOverlayAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Asset References")
	TSubclassOf<ANodeSelector> NodeSelectorAsset;

	UPROPERTY()
	UUserWidget* HUDReference;

	UPROPERTY()
	AMyCamera* MyCamera;

	UPROPERTY()
	ANodeSelector* NodeSelector;

	UPROPERTY()
	AMyPlayerController* OwningPC;

	FVector MousePosition;

	FVector MouseDirection;

	FPlane GroundPlane; 

public:

	UPROPERTY(BlueprintAssignable)
	FUpdatePlayerStateDelegate UpdateUIPlayerState;

	// Cache reference to owning player controller at start
	void RegisterPlayerController();

	// Spawns camera and sets as view target for the player controller
	void SpawnCamera();

	// Create HUD and add to viewport of player controller
	void SpawnHUD();

	// Create NodeSelector
	void SpawnNodeSelector();

	// Return mouse position on the ground plane
	FVector GetMousePosition();

	// Update player state
	void UpdatePlayerState(EPlayerState InputPlayerState);

	// Update what node is currently hovered over
	void UpdateHoverNode(UMyNode* InputNode);

};
