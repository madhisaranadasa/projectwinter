// Fill out your copyright notice in the Description page of Project Settings.


#include "DisplayManager.h"
#include "MyPlayerController.h"
#include "MyCamera.h"
#include "Blueprint/UserWidget.h"
#include "../Grid/NodeSelector.h"

UDisplayManager::UDisplayManager()
{
	PrimaryComponentTick.bCanEverTick = false;

	// Define the ground plane for raycasts
	GroundPlane = FPlane(FVector::ZeroVector, FVector::UpVector);
}


void UDisplayManager::BeginPlay()
{
	Super::BeginPlay();
}


void UDisplayManager::RegisterPlayerController()
{
	OwningPC = Cast<AMyPlayerController>(GetOwner());

	if (!OwningPC)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to register owning player controller!"));
	}
}


void UDisplayManager::SpawnCamera()
{

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	if (CameraPawnAsset)
	{
		AMyCamera* MyCameraDefault = CameraPawnAsset.GetDefaultObject();

		MyCamera = GetWorld()->SpawnActor<AMyCamera>(CameraPawnAsset, MyCameraDefault->StartLocation, MyCameraDefault->StartRotation, SpawnInfo);

		OwningPC->SetViewTarget(MyCamera);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Camera is not assigned on display manager"));
	}
}


void UDisplayManager::SpawnHUD()
{
	if (HUDOverlayAsset)
	{
		// Adding HUDOverlay to be rendered
		// This overload adds it to the player controller (OwningPC)
		HUDReference = CreateWidget<UUserWidget>(OwningPC, HUDOverlayAsset);
		HUDReference->AddToViewport();
		HUDReference->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("HUD is not assigned on display manager"));
	}
}


void UDisplayManager::SpawnNodeSelector()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if (NodeSelectorAsset)
	{
		ANodeSelector* NodeSelectorDefault = NodeSelectorAsset.GetDefaultObject();

		NodeSelector = GetWorld()->SpawnActor<ANodeSelector>(NodeSelectorAsset, NodeSelectorDefault->StartLocation, NodeSelectorDefault->StartRotation, SpawnInfo);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Node selector not assigned on display manager"));
	}
}


FVector UDisplayManager::GetMousePosition()
{
	FVector MousePoint = FVector::ZeroVector;

	if (MyCamera)
	{
		// Draw a ray from camera through mouse to the ground plane and find the intersection point
		OwningPC->DeprojectMousePositionToWorld(MousePosition, MouseDirection);
		FVector RayOrigin = MyCamera->GetActorLocation();
		FVector RayDirection = MousePosition - RayOrigin;

		MousePoint = FMath::RayPlaneIntersection(RayOrigin, RayDirection, GroundPlane);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to find mouse position without a camera, defaulting to zero vector"));
	}

	return MousePoint;
}


void UDisplayManager::UpdatePlayerState(EPlayerState InputPlayerState)
{
	UpdateUIPlayerState.Broadcast(InputPlayerState);
}


void UDisplayManager::UpdateHoverNode(UMyNode* InputNode)
{
	NodeSelector->UpdateState(InputNode);
}

