// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "GameModeDefault.h"
#include "../Grid/GridManager.h"
#include "../Grid/MyNode.h"
#include "../Grid/NodeActor.h"
#include "Blueprint/UserWidget.h"
#include "DisplayManager.h"

AMyPlayerController::AMyPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
	bShowMouseCursor = true;

	// Create display manager
	DisplayManager = CreateDefaultSubobject<UDisplayManager>(TEXT("DisplayManager"));

	// Set up starting player parameters
	CurrentState = EPlayerState::Free;
	SelectedNode = nullptr;
}


void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();

	DisplayManager->RegisterPlayerController();
	DisplayManager->SpawnCamera();
	DisplayManager->SpawnHUD();
	DisplayManager->SpawnNodeSelector();

	RegisterGrid();
	ChangeStateToFree();
}


void AMyPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	InputComponent->BindAction("LeftMouse", IE_Pressed, this, &AMyPlayerController::OnLeftMousePress);
	InputComponent->BindAction("RightMouse", IE_Pressed, this, &AMyPlayerController::OnRightMousePress);
	InputComponent->BindAction("Spacebar", IE_Pressed, this, &AMyPlayerController::OnSpaceBarPress);
}


void AMyPlayerController::OnLeftMousePress()
{
	UE_LOG(LogTemp, Warning, TEXT("Left Click"));

	// Determine mouse position
	FVector MousePosition = DisplayManager->GetMousePosition();

	// Get underlying node
	UMyNode* ClickedNode = GridManager->GetNodeFromPoint(MousePosition);
	


	if (CurrentState == EPlayerState::Free)
	{
		if (ClickedNode->IsFree())
		{
			ChangeStateToHolding(ClickedNode);
		}
	}
	else if (CurrentState == EPlayerState::Holding)
	{
		if (ClickedNode->IsFree())
		{
			GridManager->FindPath(ClickedNode, SelectedNode);
			ChangeStateToFree();
		}
	}

	DrawNeighbors(ClickedNode);

	//// Figure out what to do
	//if (CurrentState == EPlayerState::Free)
	//{
	//	if (ClickedNode->IsWalkable())
	//	{
	//		if (ClickedNode->IsFree())
	//		{
	//			GridManager->SpawnNodeActor(ClickedNode, DefaultNodeActorAsset);
	//		}
	//		else
	//		{
	//			ChangeStateToHolding(ClickedNode);
	//		}
	//	}
	//	
	//}
	//else if (CurrentState == EPlayerState::Holding)
	//{
	//	if (ClickedNode->IsWalkable())
	//	{
	//		if (ClickedNode->IsFree())
	//		{
	//			GridManager->MoveNodeActor(SelectedNode->GetNodeActor(), ClickedNode);
	//			ChangeStateToFree();
	//		}
	//	}
	//}

}


void AMyPlayerController::OnRightMousePress()
{
	UE_LOG(LogTemp, Warning, TEXT("Right Click"));
}


void AMyPlayerController::OnSpaceBarPress()
{
	UE_LOG(LogTemp, Warning, TEXT("Spacebar Press"));

	GridManager->DisplayGrid();
}


void AMyPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	HandleHover();
}


void AMyPlayerController::RegisterGrid()
{
	AGameModeDefault* CurrentGameMode = Cast<AGameModeDefault>(UGameplayStatics::GetGameMode(this));

	if (CurrentGameMode)
	{
		GridManager = CurrentGameMode->GetCurrentGrid();

		if (!GridManager)
		{
			UE_LOG(LogTemp, Warning, TEXT("For some reason, getting the grid failed!"));
		}
	}
}


void AMyPlayerController::SpawnActorAtNode(UMyNode* InputNode, TSubclassOf<ANodeActor> NodeActorAsset)
{

	if (!InputNode)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn actor at node failed in player controller because node is not valid"));
		return;
	}

	if (!NodeActorAsset)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawn actor at node failed in player controller because node actor asset is not valid"));
		return;
	}

	GridManager->SpawnNodeActor(InputNode, NodeActorAsset);

}


void AMyPlayerController::ChangeStateToHolding(UMyNode* InputNode)
{
	SelectedNode = InputNode;
	CurrentState = EPlayerState::Holding;
	DisplayManager->UpdatePlayerState(CurrentState);
}


void AMyPlayerController::ChangeStateToFree()
{
	SelectedNode = nullptr;
	CurrentState = EPlayerState::Free;
	DisplayManager->UpdatePlayerState(CurrentState);
}


void AMyPlayerController::HandleHover()
{
	int32 NewW;
	int32 NewH;

	GridManager->GetCoordFromPoint(DisplayManager->GetMousePosition(), NewW, NewH);

	if (NewH != CurrentH || NewW != CurrentW)
	{
		CurrentH = NewH;
		CurrentW = NewW;

		UMyNode* NewNode = GridManager->GetNodeFromCoord(CurrentW, CurrentH);
		DisplayManager->UpdateHoverNode(NewNode);
	}
}


void AMyPlayerController::DrawNeighbors(UMyNode* InputNode)
{
	TArray<UMyNode*> Neighbors = GridManager->GetNeighbors(InputNode);

	for (int32 i = 0; i < Neighbors.Num(); i++)
	{
		FVector NodePosition = Neighbors[i]->GetWorldPosition();
		DrawDebugBox(GetWorld(), NodePosition, FVector(10.0f, 10.0f, 10.0f), FColor::Green, false, 3.0f, 0, 3.0f);
	}
}
