// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameModeDefault.generated.h"

class AGridManager;
 
UCLASS()
class PROJECTWINTER_API AGameModeDefault : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	// Method to return the current grid to anyone that wants the reference
	AGridManager* GetCurrentGrid();
};
