// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModeDefault.h"
#include "../Grid/GridManager.h"

AGridManager* AGameModeDefault::GetCurrentGrid()
{
	AGridManager* OutGrid = nullptr;
	
	TArray<AActor*> AllChildActors;
	GetAllChildActors(AllChildActors);

	for (AActor* ChildActor : AllChildActors)
	{
		OutGrid = Cast<AGridManager>(ChildActor);

		if(OutGrid)
		{
			break;
		}

	}

	return OutGrid;
}
