// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"


class ANodeActor;
class UMyNode;
class AGridManager;
class UDisplayManager;

UENUM(BlueprintType)
enum class EPlayerState : uint8 {
	Free			UMETA(DisplayName = "Free"),
	Holding			UMETA(DisplayName = "Holding")
};

UCLASS()
class PROJECTWINTER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AMyPlayerController();

protected:

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	virtual void PlayerTick(float DeltaTime) override;

protected:

	UPROPERTY(EditAnywhere, Category = "Components")
	UDisplayManager* DisplayManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup | Blueprint References")
	TSubclassOf<ANodeActor> DefaultNodeActorAsset;

	UPROPERTY()
	AGridManager* GridManager;

	UPROPERTY()
	UMyNode* SelectedNode;

	UPROPERTY(VisibleAnywhere, Category = "Parameters")
	EPlayerState CurrentState;

	int32 CurrentW;

	int32 CurrentH;

protected:

	// Register grid from the game mode
	void RegisterGrid();

	// Bound action for left mouse press
	void OnLeftMousePress();

	// Bound action or right mouse press
	void OnRightMousePress();

	// Bound action for space bar press
	void OnSpaceBarPress();

	// Request a spawn from the grid manager
	void SpawnActorAtNode(UMyNode* InputNode, TSubclassOf<ANodeActor> NodeActorAsset);

	// Change state to holding
	void ChangeStateToHolding(UMyNode* InputNode);

	// Change state to free
	void ChangeStateToFree();

	// Handles what happens when the mouse hovers over a new node
	void HandleHover();

	// DEBUG - Draw neighbors of the input node
	void DrawNeighbors(UMyNode* InputNode);

};
